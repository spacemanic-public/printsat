# PrintSat

**Download and print your own CubeSat!**

This is not just a toy. Every component has dimensions and parameters true to real components from [Spacemanic](http://spacemanic.com) portfolio. We are using this PrintSat during design phases with our customers to accommodate their payload with our platform.

At the moment there is a 1U structure and 2U structure with corresponding solar panel designs.

## Folder structure
*  **Antenna placeholder** - a simple model of an antenna system
*  **Assembly** - an example of an assembled CubeSat
*  **Magnetorquer** - magnetorquer module with Spacemanic LEMA coils
*  **OBC Eddie** - simplified model of Spacemanic's OBC Eddie module
*  **Payloads** - placeholders for payload modules
*  **PC104 motherboard** - PC104 standardized motherboards for Spacemanic modules
*  **PSU** - Power Supply Unit board with batteries and battery holders
*  **Solar panels** - standard solar panel models
*  **Structure** - CubeSat rails slightly modified for easy printing
*  **Supports** - distance rings and other useful parts
*  **TRX Murgas** - simplified model of Spacemanic's TRX Murgas module

## Assembly
The assembly is really straightforward same as a real CubeSat! Just stack the modules on top of each other.

![CubeSat assembly](images/Assembly-annotation.png "CubeSat assembly")

For full assembly you need a few more parts from your local hardware store:
*  M3 x 85mm rod in the corners
*  M2.5 x 6mm Countersunk screws for Structure assembly and solar panels
*  M2.5 x 6mm Button head screws to fix modules to the motherboard

The screws will cut their own thread, do not be afraid to use a *little* force.

**Suggestions for improvement welcome!**