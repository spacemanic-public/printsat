# Structure assembly

Use **M2.5 x 6mm** Countersunk screws for the structure assembly.

![Structure](images/Structure-annotation.png "Structure")

![Structure assembly video](images/Structure-assembly-video.mp4 "Structure assembly video")