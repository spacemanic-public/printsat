# Magnetorquer assembly

For height compatible module use three **Distance rings 9.24mm** on top of Corner A/B/C and one **Distance ring 15.24mm** in the remaining spot.

Use **M2.5 x 6mm** Button head screws to fix the Coil Z module to the Magnetorquer board.

![Magnetorquer module](images/Magnetorquer-annotation.png "Magnetorquer module")